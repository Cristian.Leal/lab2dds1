public class Kata {

    public static String solution(String str) {
        String reverseStr = "";
        for (int i = str.length(); i > 0; i--) {
            reverseStr += str.charAt(i-1);
        }
        return reverseStr;
    }

}