public class Game {

    public String winner(String[] deckSteve, String[] deckJosh) {
        String deck[] = {"2","3","4","5","6","7","8","9","T","J","Q","K","A"};
        String result = "";
        int round = 0, Stevewins = 0, JoshWins = 0;

        for (int i = 0; i < deckSteve.length; i++) {
            int posCardSteve = 0, posCardJosh = 0;

            do{
                if (deck[posCardSteve] != deckSteve[round]) {
                    posCardSteve++;
                }
                if (deck[posCardJosh] != deckJosh[round]) {
                    posCardJosh++;
                }
            } while ((deck[posCardSteve] != deckSteve[round]) || (deck[posCardJosh] != deckJosh[round]));

            if (posCardSteve > posCardJosh) {
                Stevewins++;
            } else if(posCardSteve < posCardJosh){
                JoshWins++;
            }

            round++;
        }

        if (Stevewins > JoshWins) {
            result = "Steve wins "+Stevewins+" to "+JoshWins;
        } else if (Stevewins < JoshWins) {
            result = "Josh  wins "+JoshWins+" to "+Stevewins;
        } else {
            result = "Tie";
        }

        return result;
    }
}